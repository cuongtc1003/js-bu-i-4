// bai1:
/*
sắp xep so theo thu tu tang dan

Bước 1: tao biến num1,num2,num3
bước 2:  sắp xếp theo thứ tự tăng dần
bước 3: in kêt quả ra màn hình
*/
function sapxepso(){
    var num1 = document.getElementById("txt-so-thu-1").value *1;
    var num2 = document.getElementById("txt-so-thu-2").value *1;
    var num3 = document.getElementById("txt-so-thu-3").value *1;
//number 1
if(num1 < num2 && num1 < num3){   
    if(num2 <= num3){
        document.getElementById("sortnumber").innerHTML=`${num1} ${num2} ${num3}`;
    }else{
        document.getElementById("sortnumber").innerHTML=`${num1} ${num3} ${num2}`;
    }
}else if(num2 < num3 && num2 < num1){
    if(num3 <= num1){
        document.getElementById("sortnumber").innerHTML=`${num2} ${num3} ${num1}`;
    }else{
        document.getElementById("sortnumber").innerHTML=`${num2} ${num1} ${num3}`;
    }
}else if(num3 < num1 && num3 < num2){
    if(num1 <= num2 ){
        document.getElementById("sortnumber").innerHTML=`${num3} ${num1} ${num2}`;
    }else{
        document.getElementById("sortnumber").innerHTML=`${num3} ${num2} ${num1}`;
    }
}else{
    document.getElementById("sortnumber").innerHTML = `${num3} ${num1} ${num2}`
}
}
//bai 2 viết chương trình chào hỏi 
/*
    bước 1:tao biến thanhvien,father,mother,brother,Brother
    bước 2:set lời chao cho từng phần tử
    bước 3:xuất lời chào ra màn hình
    */
function sentHello(){
    var thanhvien = document.getElementById("thanhvien").value;
    var father = document.getElementById('txt-father').value;
    var mother = document.getElementById('txt-mom').value;
    var brother = document.getElementById('txt-brother').value;
    var Brother = document.getElementById('txt-Brother').value;
if(thanhvien === father){
    document.getElementById("txt-Hello").innerHTML=`Xin Chào Bố`;
}else if(thanhvien == mother){
    document.getElementById("txt-Hello").innerHTML=`Xin Chào Mẹ`;
}else if(thanhvien == brother){
    document.getElementById("txt-Hello").innerHTML=`Xin Chào Em Trai`;
}else if(thanhvien == Brother){
    document.getElementById("txt-Hello").innerHTML=`Xin Chào Anh Trai`;
}
}
//bai3 :Đếm số chẵn lẻ
/*
Bước 1:tạo biến firstNumber,secondNumber,thirstNumber
Bước 2:xử lý số chắn,số lẻ
Bước 3;xuất dữ liệu ra màn hình
*/
function sochanle(){
    var firstNumber = document.getElementById("txt_so_1").value;
    var secondNumber = document.getElementById("txt_so_2").value;
    var thirstNumber =document.getElementById("txt_so_3").value;
    var soChan = 0;
    var soLe = 0;
if(firstNumber <= 0 || secondNumber <= 0 || thirstNumber <= 0){
    document.getElementById("result_chanle").innerText=`Nhập Sai Nội Dung`
}else{
    if(firstNumber % 2 == 0){
        soChan++;
    }
    if(secondNumber % 2 == 0){
        soChan++;
    }
    if(thirstNumber % 2 == 0){
        soChan++;
    }
    soLe = 3 - soChan;
document.getElementById("result_chanle").innerText=` Số chẵn:${soChan}
                                                    Số lẻ:${soLe}`
} 
}

// bai 4: nhập 3 cạnh cho biết tam giác gì?
/*
Bước 1: Tạo biến edge1,edge2,edge3 và ketqua
Bước 2: áp dụng công thức xác định tam giác vuông,cân,đều
Bước 3: In kết quả ra màn hình
*/
function xuattamgiac(){
    var edge1 = document.getElementById("txt-canh-1").value*1;
    var edge2 = document.getElementById("txt-canh-2").value*1;
    var edge3 = document.getElementById("txt-canh-3").value*1;
    ketqua="";
    if(edge1<=0 || edge2<=0 || edge3<=0){
        document.getElementById("ketquatamgiac").innerText=`Không hợp lệ!`
    }else{
       if(edge1 >0 || edge2 > 0 || edge3 > 0){
        if( edge1 == edge2 && edge2 == edge3 ){
            ketqua="Tam giác đều"
        }else if(edge1 == edge2 || edge1 == edge3 || edge2 == edge3){
            ketqua= "Tam giác cân"
        }
        else if(edge1*edge1 == (edge2*edge2 + edge3*edge3)){
            ketqua = "Tam giác vuông"
        }else{
            ketqua = "Tam giác khác"
        }
       } 
    }
    document.getElementById("ketquatamgiac").innerText=`${ketqua}`
}
